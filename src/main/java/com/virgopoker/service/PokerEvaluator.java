package com.virgopoker.service;

import com.virgopoker.model.Card;
import com.virgopoker.model.Result;
import com.virgopoker.model.ResultHand;

import java.util.*;
import java.util.stream.Collectors;

public class PokerEvaluator {

    SortedMap<Card.CardValue, List<Card>> valueMap;

    public PokerEvaluator(List<Card> cards) {
        convertToValueMap(cards);
    }

    public ResultHand evaluate() {
        ResultHand resultHand = new ResultHand();

        Optional<List<Card>> possiblePairs = detectPairs();
        if (possiblePairs.isPresent()) {
            if (valueMap.size() == 4) {
                resultHand.setResult(Result.ONE_PAIR);
            }
            if (valueMap.size() == 3) {
                if (possiblePairs.get().size() == 3) {
                    resultHand.setResult(Result.THREE_OF_A_KIND);
                } else {
                    resultHand.setResult(Result.TWO_PAIR);
                }
            }
            if (valueMap.size() == 2) {
                if (possiblePairs.get().size() == 5) {
                    resultHand.setResult(Result.FULL_HOUSE);
                } else {
                    resultHand.setResult(Result.FOUR_OF_A_KIND);
                }
            }
            resultHand.setResultCards(possiblePairs.get());
        }

        Optional<List<Card>> possibleFlush = detectFlush();
        Optional<List<Card>> possibleStraight = detectStraight();

        if (possibleFlush.isPresent() && possibleStraight.isPresent()) {
            resultHand.setResult(Result.STRAIGHT_FLUSH);
            resultHand.setResultCards(possibleFlush.get());
            return resultHand;
        }

        if (possibleStraight.isPresent()) {
            resultHand.setResult(Result.STRAIGHT);
            resultHand.setResultCards(possibleStraight.get());
        }
        if (possibleFlush.isPresent()) {
            resultHand.setResult(Result.FLUSH);
            resultHand.setResultCards(possibleFlush.get());
        }

        if (resultHand.getResult() == null) {
            resultHand.setResult(Result.HIGH_CARD);
            resultHand.setResultCards(valueMap.get(valueMap.lastKey()));
        }
        return resultHand;
    }

    public Optional<List<Card>> detectPairs() {
        if (valueMap.size() == 5) {
            return Optional.empty();
        }

        List<List<Card>> collect = valueMap.entrySet().stream().filter(entry -> entry.getValue().size() > 1).map(Map.Entry::getValue).collect(Collectors.toList());
        return Optional.of(collect.stream().collect(ArrayList::new, List::addAll, List::addAll));

    }

    public Optional<List<Card>> detectFlush() {
        Card.CardType type = null;
        for (Map.Entry<Card.CardValue, List<Card>> entry : valueMap.entrySet()) {
            for (Card card : entry.getValue()) {
                if (type != null && card.getType() != type) {
                    return Optional.empty();
                }
                type = card.getType();
            }
        }
        return Optional.of(valueMap.values().stream().collect(ArrayList::new, List::addAll, List::addAll));
    }

    public Optional<List<Card>> detectStraight() {
        int prevValue = 0;
        for (Map.Entry<Card.CardValue, List<Card>> entry : valueMap.entrySet()) {
            for (Card card : entry.getValue()) {
                if (prevValue != 0 && card.getValue().getValue() - 1 != prevValue) {
                    return Optional.empty();
                }
                prevValue = card.getValue().getValue();
            }
        }
        return Optional.of(valueMap.values().stream().collect(ArrayList::new, List::addAll, List::addAll));
    }

    private void convertToValueMap(List<Card> cards) {
        Collections.sort(cards);
        valueMap = new TreeMap<>();
        for (Card card : cards) {
            if (valueMap.containsKey(card.getValue())) {
                valueMap.get(card.getValue()).add(card);
            } else {
                valueMap.put(card.getValue(), new ArrayList<>(Arrays.asList(card)));
            }
        }
    }
}
