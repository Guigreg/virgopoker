package com.virgopoker.service;

import com.virgopoker.model.Card;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class DeckUtils {

    public List<Card> generate() {
        List<Card> deck = new ArrayList<>();
        for (Card.CardType type : Card.CardType.values()) {
            for (Card.CardValue value : Card.CardValue.values()) {
                deck.add(new Card(type, value));
            }
        }
        return deck;
    }

    public List<Card> getRandomFiveCards(List<Card> cards) {
        List<Card> output = new ArrayList<>();

        int randomNum;
        while (output.size() != 5) {
            randomNum = ThreadLocalRandom.current().nextInt(0, 52);
            if (!output.contains(cards.get(randomNum)))
                output.add(cards.get(randomNum));
        }
        return output;
    }

    public void printHand(List<Card> cards) {
        cards.forEach(System.out::println);
    }
}
