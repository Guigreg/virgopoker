package com.virgopoker;

import com.virgopoker.model.Card;
import com.virgopoker.model.ResultHand;
import com.virgopoker.service.DeckUtils;
import com.virgopoker.service.PokerEvaluator;

import java.util.List;

public class Poker {
    public static void main(String[] args) {
        DeckUtils utils = new DeckUtils();

        List<Card> cards = utils.getRandomFiveCards(utils.generate());
        utils.printHand(cards);

        System.out.println(" ------ ");

        PokerEvaluator poker = new PokerEvaluator(cards);
        ResultHand resultHand = poker.evaluate();

        System.out.println(resultHand.getResult());
        resultHand.getResultCards().forEach(System.out::println);
    }
}
