package com.virgopoker.model;

import java.util.List;

public class ResultHand {
    private List<Card> resultCards;
    private Result result;

    public List<Card> getResultCards() {
        return resultCards;
    }

    public void setResultCards(List<Card> resultCards) {
        this.resultCards = resultCards;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
