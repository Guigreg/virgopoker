package com.virgopoker.model;

public class Card implements Comparable {

    private CardType type;
    private CardValue value;

    public Card(CardType type, CardValue value) {
        this.type = type;
        this.value = value;
    }

    public CardType getType() {
        return type;
    }

    public CardValue getValue() {
        return value;
    }

    @Override
    public int compareTo(Object o) {
        if (this == o) return 0;
        if (!(o instanceof Card)) return -1;

        Card card = (Card) o;

        return getValue().getValue().compareTo(card.getValue().getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card)) return false;

        Card card = (Card) o;

        return type == card.type && value == card.value;
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Card{" +
                "type=" + type +
                ", value=" + value +
                '}';
    }

    public enum CardType {HEARTS, SPADES, DIAMONDS, CLUBS}

    public enum CardValue {
        TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10), JACK(11), QUEEN(12), KING(13), ACE(14);

        private Integer value;

        CardValue(int value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }
    }
}
