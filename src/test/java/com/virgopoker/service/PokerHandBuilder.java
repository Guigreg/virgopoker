package com.virgopoker.service;

import com.virgopoker.model.Card;

import java.util.ArrayList;
import java.util.List;

public class PokerHandBuilder {

    PokerEvaluator makeDeckWithFlush() {
        List<Card> cardList = new ArrayList<>();

        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.FIVE));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.ACE));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.FOUR));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.JACK));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.THREE));

        return new PokerEvaluator(cardList);
    }

    PokerEvaluator makeDeckWithWrongFlush() {
        List<Card> cardList = new ArrayList<>();

        cardList.add(new Card(Card.CardType.SPADES, Card.CardValue.FIVE));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.ACE));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.FOUR));
        cardList.add(new Card(Card.CardType.DIAMONDS, Card.CardValue.JACK));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.THREE));

        return new PokerEvaluator(cardList);
    }

    PokerEvaluator makeDeckWithStraight() {
        List<Card> cardList = new ArrayList<>();

        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.FIVE));
        cardList.add(new Card(Card.CardType.CLUBS, Card.CardValue.SIX));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.FOUR));
        cardList.add(new Card(Card.CardType.SPADES, Card.CardValue.TWO));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.THREE));

        return new PokerEvaluator(cardList);
    }

    PokerEvaluator makeDeckWithWrongStraight() {
        List<Card> cardList = new ArrayList<>();

        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.FIVE));
        cardList.add(new Card(Card.CardType.CLUBS, Card.CardValue.FIVE));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.FOUR));
        cardList.add(new Card(Card.CardType.SPADES, Card.CardValue.TWO));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.THREE));

        return new PokerEvaluator(cardList);
    }

    PokerEvaluator makeDeckWithAPair() {
        List<Card> cardList = new ArrayList<>();

        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.CLUBS, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.ACE));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.QUEEN));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.TWO));

        return new PokerEvaluator(cardList);
    }

    PokerEvaluator makeDeckWithTwoPairs() {
        List<Card> cardList = new ArrayList<>();

        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.CLUBS, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.ACE));
        cardList.add(new Card(Card.CardType.CLUBS, Card.CardValue.ACE));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.TWO));

        return new PokerEvaluator(cardList);
    }

    PokerEvaluator makeDeckWithThreeOfAKind() {
        List<Card> cardList = new ArrayList<>();

        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.CLUBS, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.SPADES, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.CLUBS, Card.CardValue.ACE));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.TWO));

        return new PokerEvaluator(cardList);
    }

    PokerEvaluator makeDeckWithFullHouse() {
        List<Card> cardList = new ArrayList<>();

        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.CLUBS, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.SPADES, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.CLUBS, Card.CardValue.ACE));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.ACE));

        return new PokerEvaluator(cardList);
    }

    PokerEvaluator makeDeckWithFourOfAKind() {
        List<Card> cardList = new ArrayList<>();

        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.CLUBS, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.SPADES, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.DIAMONDS, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.ACE));

        return new PokerEvaluator(cardList);
    }

    PokerEvaluator makeDeckWithHighCard() {
        List<Card> cardList = new ArrayList<>();

        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.KING));
        cardList.add(new Card(Card.CardType.CLUBS, Card.CardValue.TWO));
        cardList.add(new Card(Card.CardType.SPADES, Card.CardValue.SIX));
        cardList.add(new Card(Card.CardType.DIAMONDS, Card.CardValue.THREE));
        cardList.add(new Card(Card.CardType.HEARTS, Card.CardValue.ACE));

        return new PokerEvaluator(cardList);
    }
}
