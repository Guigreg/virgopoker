package com.virgopoker.service;

import com.virgopoker.model.Card;
import com.virgopoker.model.Result;
import com.virgopoker.model.ResultHand;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class PokerEvaluatorTest extends PokerHandBuilder {

    @Test
    public void shouldDetectFlush() throws Exception {
        PokerEvaluator underTest = makeDeckWithFlush();

        ResultHand actual = underTest.evaluate();

        assertEquals(5, actual.getResultCards().size());
        assertEquals(Result.FLUSH, actual.getResult());
    }

    @Test
    public void shouldReturnEmptyOnWrongFlush() throws Exception {
        PokerEvaluator underTest = makeDeckWithWrongFlush();

        Optional<List<Card>> cards = underTest.detectFlush();

        assertFalse(cards.isPresent());
    }

    @Test
    public void shouldReturnEmptyOnWrongStraight() throws Exception {
        PokerEvaluator underTest = makeDeckWithWrongStraight();

        Optional<List<Card>> cards = underTest.detectStraight();

        assertFalse(cards.isPresent());
    }

    @Test
    public void shouldDetectStraight() throws Exception {
        PokerEvaluator underTest = makeDeckWithStraight();

        ResultHand actual = underTest.evaluate();

        assertEquals(5, actual.getResultCards().size());
        assertEquals(Result.STRAIGHT, actual.getResult());
    }

    @Test
    public void detectPairs() throws Exception {
        PokerEvaluator underTest = makeDeckWithAPair();

        ResultHand actual = underTest.evaluate();

        assertEquals(2, actual.getResultCards().size());
        assertEquals(Result.ONE_PAIR, actual.getResult());
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.HEARTS, Card.CardValue.KING)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.CLUBS, Card.CardValue.KING)));
    }

    @Test
    public void detectTwoPairs() throws Exception {
        PokerEvaluator underTest = makeDeckWithTwoPairs();

        ResultHand actual = underTest.evaluate();

        assertEquals(4, actual.getResultCards().size());
        assertEquals(Result.TWO_PAIR, actual.getResult());
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.HEARTS, Card.CardValue.KING)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.CLUBS, Card.CardValue.KING)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.HEARTS, Card.CardValue.ACE)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.CLUBS, Card.CardValue.ACE)));
    }

    @Test
    public void detectThreeOfAKind() throws Exception {
        PokerEvaluator underTest = makeDeckWithThreeOfAKind();

        ResultHand actual = underTest.evaluate();

        assertEquals(3, actual.getResultCards().size());
        assertEquals(Result.THREE_OF_A_KIND, actual.getResult());
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.HEARTS, Card.CardValue.KING)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.CLUBS, Card.CardValue.KING)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.SPADES, Card.CardValue.KING)));
    }

    @Test
    public void detectFullHouse() throws Exception {
        PokerEvaluator underTest = makeDeckWithFullHouse();

        ResultHand actual = underTest.evaluate();

        assertEquals(5, actual.getResultCards().size());
        assertEquals(Result.FULL_HOUSE, actual.getResult());
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.HEARTS, Card.CardValue.KING)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.CLUBS, Card.CardValue.KING)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.SPADES, Card.CardValue.KING)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.CLUBS, Card.CardValue.ACE)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.HEARTS, Card.CardValue.ACE)));
    }

    @Test
    public void detectFourOfAKind() throws Exception {
        PokerEvaluator underTest = makeDeckWithFourOfAKind();

        ResultHand actual = underTest.evaluate();

        assertEquals(4, actual.getResultCards().size());
        assertEquals(Result.FOUR_OF_A_KIND, actual.getResult());
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.HEARTS, Card.CardValue.KING)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.CLUBS, Card.CardValue.KING)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.SPADES, Card.CardValue.KING)));
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.DIAMONDS, Card.CardValue.KING)));
    }

    @Test
    public void detectHighCard() throws Exception {
        PokerEvaluator underTest = makeDeckWithHighCard();

        ResultHand actual = underTest.evaluate();

        assertEquals(1, actual.getResultCards().size());
        assertEquals(Result.HIGH_CARD, actual.getResult());
        assertTrue(actual.getResultCards().contains(new Card(Card.CardType.HEARTS, Card.CardValue.ACE)));
    }


}