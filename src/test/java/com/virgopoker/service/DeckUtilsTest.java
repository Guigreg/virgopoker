package com.virgopoker.service;

import com.virgopoker.model.Card;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;

public class DeckUtilsTest {
    DeckUtils underTest = new DeckUtils();

    @Test
    public void shouldGenerateAllCards() throws Exception {
        List<Card> deck = underTest.generate();

        assertEquals(deck.size(), 52);
        assertTrue(deck.contains(new Card(Card.CardType.HEARTS, Card.CardValue.KING)));
    }

    @Test
    public void shouldReturnFiveCards() throws Exception {
        List<Card> randomFiveCards = underTest.getRandomFiveCards(underTest.generate());

        assertEquals(randomFiveCards.size(), 5);
        assertTrue(notTheSame(randomFiveCards));
    }

    private boolean notTheSame(List<Card> randomFiveCards) {
        return new HashSet<>(randomFiveCards).size() == randomFiveCards.size();
    }
}